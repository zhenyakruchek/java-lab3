package Modules;

public interface PurelyFunctionalSet<T> {
    boolean contains(T element);
}
