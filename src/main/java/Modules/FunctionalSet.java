package Modules;

import java.util.Objects;
import java.util.function.Predicate;

public class FunctionalSet {

    public static <T> PurelyFunctionalSet<T> empty() {
        return x -> false;
    }

    public static <T> PurelyFunctionalSet<T> singletonSet(T val) {
        return x -> Objects.equals(val, x);
    }

    public static <T> PurelyFunctionalSet<T> union(PurelyFunctionalSet<T> s, PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) || t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> intersect(PurelyFunctionalSet<T> s, PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) && t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> diff(PurelyFunctionalSet<T> s, PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) && !t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> filter(PurelyFunctionalSet<T> s, Predicate<T> p) {
        return x -> s.contains(x) && p.test(x);
    }
}
