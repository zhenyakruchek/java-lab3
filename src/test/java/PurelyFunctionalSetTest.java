import Modules.PurelyFunctionalSet;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import Modules.*;
import java.util.function.Function;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static Modules.FunctionalSet.*;
import static org.junit.Assert.*;

@DisplayName("Functional Set Utils")
public class PurelyFunctionalSetTest {
        @Test
        @DisplayName("Empty set test")
        public void emptySetTest() {
            PurelyFunctionalSet<Integer> set = empty();
            assertNotNull(set);
            assertFalse(set.contains(null));
            IntStream.range(-10, 11).forEach(x -> assertFalse(set.contains(x)));

            PurelyFunctionalSet<Character> characterSet = empty();
            assertNotNull(characterSet);
            assertFalse(characterSet.contains(null));
            assertFalse(characterSet.contains('\0'));
            assertFalse(characterSet.contains('a'));
            assertFalse(characterSet.contains('Q'));
            assertFalse(characterSet.contains('%'));
        }

        @Test
        @DisplayName("Singleton set test")
        public void singletonSetTest() {
            PurelyFunctionalSet<Integer> set = singletonSet(0);
            IntStream.range(-10, 11).forEach(x -> assertEquals(set.contains(x), x == 0));

            PurelyFunctionalSet<Object> stringSetOfNull = singletonSet(null);
            assertTrue(stringSetOfNull.contains(null));

            PurelyFunctionalSet<String> stringSet = singletonSet("");
            assertTrue(stringSet.contains(""));

            StringBuilder sb = new StringBuilder().append(this.hashCode()).insert(0, this.toString());
            PurelyFunctionalSet<String> stringSetFromSb = singletonSet(sb.toString());
            assertTrue(stringSetFromSb.contains(this.toString() + this.hashCode()));

            PurelyFunctionalSet<Character> characterSet = singletonSet('C');
            assertTrue(characterSet.contains('C'));
            assertFalse(characterSet.contains('c'));
        }

        @Test
        @DisplayName("Union test")
        public void unionTest() {
            PurelyFunctionalSet unionSame = union(singletonSet("TesT"), singletonSet("test"));
            assertTrue(unionSame.contains("TesT"));
            assertTrue(unionSame.contains("test"));

            int[] numbers = new int [] { -657, 34 };
            PurelyFunctionalSet<Integer> unionNumber = union(singletonSet(numbers[0]), singletonSet(numbers[1]));
            assertTrue(unionNumber.contains(numbers[0]));
            assertTrue(unionNumber.contains(numbers[1]));

            PurelyFunctionalSet<Integer> unionNumberAnother = union(unionNumber, singletonSet(0));
            assertTrue(unionNumberAnother.contains(0));
            assertTrue(unionNumberAnother.contains(numbers[0]));
            assertTrue(unionNumberAnother.contains(numbers[1]));

            PurelyFunctionalSet<Integer> unionSet = convertStreamToSet(IntStream.range(-10, 11));
            IntStream.range(-10, 11).forEach(x -> assertTrue(unionSet.contains(x)));
        }

        public static <T> PurelyFunctionalSet<T> convertStreamToSet(Stream<T> stream) {
            return stream
                    .map(FunctionalSet::singletonSet)
                    .reduce(empty(), FunctionalSet::union);
        }

        public static PurelyFunctionalSet<Integer> convertStreamToSet(IntStream stream) {
            return convertStreamToSet(stream.boxed());
        }

        public static PurelyFunctionalSet<Double> convertStreamToSet(DoubleStream stream) {
            return convertStreamToSet(stream.boxed());
        }

        @Test
        @DisplayName("Intersect test")
        public void intersectTest() {
            PurelyFunctionalSet<Double> doubleSetOne = convertStreamToSet(IntStream.range(-10, 11).mapToDouble(x -> (x - 5) / 100.0));
            PurelyFunctionalSet<Double> doubleSetOneAnother = convertStreamToSet(IntStream.range(-10, 11).mapToDouble(x -> (x + 5) / 100.0));
            PurelyFunctionalSet<Double> doubleSetIntersect = intersect(doubleSetOne, doubleSetOneAnother);
            PurelyFunctionalSet<Double> doubleSetIntersectReverse = intersect(doubleSetOneAnother, doubleSetOne);
            IntStream.range(-10, 11).mapToDouble(x -> x / 100.0).forEach(x -> {
                assertEquals(doubleSetIntersect.contains(x), x >= -0.05 && x <= 0.05);
                assertEquals(doubleSetIntersect.contains(x), doubleSetIntersectReverse.contains(x));
            });

            assertTrue(intersect(singletonSet("Test!"), singletonSet("Test!")).contains("Test!"));

            char[] chars = new char[] { 'Q', 'f', '0'};
            PurelyFunctionalSet<Character> intersectChars = intersect(
                    union(singletonSet(chars[0]), singletonSet(chars[1])),
                    union(singletonSet(chars[1]), singletonSet(chars[2])));
            assertTrue(intersectChars.contains(chars[1]));
            assertFalse(intersectChars.contains(chars[0]));
            assertFalse(intersectChars.contains(chars[2]));
        }

        @Test
        @DisplayName("Diff test")
        public void diffTest() {
            PurelyFunctionalSet<Integer> intSetOne = convertStreamToSet(IntStream.range(-10, 11));
            PurelyFunctionalSet<Integer> intSetAnother = convertStreamToSet(IntStream.range(-5, 6));
            PurelyFunctionalSet<Integer> intSetDiff = diff(intSetOne, intSetAnother);
            PurelyFunctionalSet<Integer> intSetDiffReverse = diff(intSetAnother, intSetOne);
            IntStream.range(-10, 11).forEach(x -> {
                assertEquals(intSetDiff.contains(x), x < -5 || x > 5);
                assertFalse(intSetDiffReverse.contains(x));
            });

            PurelyFunctionalSet<String> diffSet = diff(singletonSet("Some string here"), singletonSet("Another String"));
            assertTrue(diffSet.contains("Some string here"));
            assertFalse(diffSet.contains("Another String"));

            char[] chars = new char[] { 'u', 'R', '\0', 'q'};
            PurelyFunctionalSet<Character> charSetOne = union(singletonSet(chars[0]), singletonSet(chars[1]));
            PurelyFunctionalSet<Character> charSetAnother = union(singletonSet(chars[2]), singletonSet(chars[3]));
            PurelyFunctionalSet<Character> diffChars = diff(union(charSetOne, charSetAnother), charSetAnother);
            assertTrue(diffChars.contains(chars[1]));
            assertTrue(diffChars.contains(chars[0]));
            assertFalse(diffChars.contains(chars[2]));
            assertFalse(diffChars.contains(chars[3]));
        }

        @Test
        @DisplayName("Filter test")
        public void filterTest() {
            PurelyFunctionalSet<Integer> intSetFiltered = filter(convertStreamToSet(IntStream.range(-10, 11)), x -> x % 2 == 0);
            IntStream.range(-10, 11).forEach(x -> assertEquals(intSetFiltered.contains(x), x % 2 == 0));

            String[] strings = new String[] { "Test", "", "&@^#@", "32", "another s", "ToM" };
            PurelyFunctionalSet<String> charSet = filter(convertStreamToSet(Stream.of(strings)), x -> x.length() > 3);
            Stream.of(strings).forEach(x -> assertEquals(charSet.contains(x), x.length() > 3));

            PurelyFunctionalSet<Object> emptySet = filter(empty(), x -> x.equals(0));
            IntStream.range(-10, 11).forEach(x -> assertFalse(emptySet.contains(x)));

            PurelyFunctionalSet<Double> singleSet = singletonSet(Math.PI);
            assertFalse(filter(singleSet, x -> x != Math.PI).contains(Math.PI));
            assertTrue(filter(singleSet, x -> true).contains(Math.PI));
        }
    }